const map = [
    [' ', ' ', 'W', 'W', 'W', 'W', 'W', ' '],
    ['W', 'W', 'W', ' ', ' ', ' ', 'W', ' '],
    ['W', 'O', 'S', 'B', ' ', ' ', 'W', ' '],
    ['W', 'W', 'W', ' ', 'B', 'O', 'W', ' '],
    ['W', 'O', 'W', 'W', 'B', ' ', 'W', ' '],
    ['W', ' ', 'W', ' ', 'O', ' ', 'W', 'W'],
    ['W', 'B', ' ', 'X', 'B', 'B', 'O', 'W'],
    ['W', ' ', ' ', ' ', 'O', ' ', ' ', 'W'],
    ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']
];
const pathArr = [];
const storages = [];

let destination = document.getElementById('map')
let player = document.getElementById('player')



function createMap() {
    for (let row = 0; row < map.length; row++) {
        for (let column = 0; column < map[row].length; column++) {
            if (map[row][column] === 'W') {
                let wall = document.createElement('div')
                wall.classList.add('wall', 'object')
                destination.appendChild(wall)
            }
            else if (map[row][column] === ' ') {
                let path = document.createElement('div')
                path.classList.add('path', 'object')
                destination.appendChild(path)
                pathArr.push(path)
            }
            else if (map[row][column] === 'S') {
                let start = document.createElement('div')
                start.classList.add('start', 'object')
                start.appendChild(player)
                destination.appendChild(start)
                pathArr.push(start)
            }
            else if (map[row][column] === 'B') {
                let path = document.createElement('div')
                path.classList.add('path', 'object')
                let box = document.createElement('div')
                box.classList.add('box', 'object')
                path.appendChild(box)
                destination.appendChild(path)
                pathArr.push(path)
                box.setAttribute('data', "class: 'box'")
            }
            else if (map[row][column] === 'O') {
                let storage = document.createElement('div')
                storage.classList.add('storage', 'object')
                destination.appendChild(storage)
                pathArr.push(storage)
            }
            else if (map[row][column] === 'X') {
                let storage = document.createElement('div')
                storage.classList.add('storage', 'object')
                let box = document.createElement('div')
                box.classList.add('box', 'object')
                storage.appendChild(box)
                destination.appendChild(storage)
                pathArr.push(storage)
                box.setAttribute('data', "class: 'box'")
            }
        }
    }
}
createMap()

function pathLoop() {
    for (let i = 0; i < pathArr.length; i++) {
        pathArr[i].classList.add(i)
    }
}
pathLoop()

function createPath() {
    let count = 0
    for (let row = 0; row < map.length; row++) {
        for (let column = 0; column < map[row].length; column++) {
            if (map[row][column] !== "W") {
                map[row][column] = pathArr[count]
                count++
            }
        }
    }
}
createPath()
console.log(map)

addEventListener('keydown', movePlayer);


let playerRow = 2;
let playerCol = 2;

function movePlayer(event) {
    if (event.code === 'ArrowRight') {

        if (map[playerRow][playerCol + 2].childElementCount === 1 && map[playerRow][playerCol + 1].childElementCount === 1 || map[playerRow][playerCol + 1] === 'W') {
            return
        }
        else {
            if (map[playerRow][playerCol + 1].childElementCount === 0) {
                map[playerRow][playerCol + 1].appendChild(player)
            }
            else if (map[playerRow][playerCol + 1].childElementCount === 1) {
                let box = map[playerRow][playerCol + 1].children[0]
                map[playerRow][playerCol + 2].appendChild(box)
                map[playerRow][playerCol + 1].appendChild(player)
                console.log(event)
            }
            playerCol++
        }
    }
    else if (event.code === 'ArrowLeft') {
     
        if (map[playerRow][playerCol - 2].childElementCount === 1 && map[playerRow][playerCol - 1].childElementCount === 1 || map[playerRow][playerCol - 1] === 'W') {
            return
        }
        else {
            if (map[playerRow][playerCol - 1].childElementCount === 0) {
                map[playerRow][playerCol - 1].appendChild(player)
            }
            else if (map[playerRow][playerCol - 1].childElementCount === 1) {
                let box = map[playerRow][playerCol - 1].children[0]
                map[playerRow][playerCol - 2].appendChild(box)
                map[playerRow][playerCol - 1].appendChild(player)
                console.log(event)
            }
            playerCol--
        }

    }
    else if (event.code === 'ArrowUp') {
        
        if (map[playerRow - 2][playerCol].childElementCount === 1 && map[playerRow - 1][playerCol].childElementCount === 1 || map[playerRow - 1][playerCol] === 'W') {
            return
        }
        else {
            if (map[playerRow - 1][playerCol].childElementCount === 0) {
                map[playerRow - 1][playerCol].appendChild(player)
            }
            else if (map[playerRow - 1][playerCol].childElementCount === 1) {
                let box = map[playerRow - 1][playerCol].children[0]
                map[playerRow - 2][playerCol].appendChild(box)
                map[playerRow - 1][playerCol].appendChild(player)
                console.log(event)
            }
            playerRow--
        }
    }
    else if (event.code === 'ArrowDown') {
      
        if (map[playerRow + 2][playerCol].childElementCount === 1 && map[playerRow + 1][playerCol].childElementCount === 1 || map[playerRow + 1][playerCol] === 'W') {
            return
        }
        else {
            if (map[playerRow + 1][playerCol].childElementCount === 0) {
                map[playerRow + 1][playerCol].appendChild(player)
            }
            else if (map[playerRow + 1][playerCol].childElementCount === 1) {
                let box = map[playerRow + 1][playerCol].children[0]
                map[playerRow + 2][playerCol].appendChild(box)
                map[playerRow + 1][playerCol].appendChild(player)
                console.log(event)
            }
            playerRow++
        }
    }
    winCheck()
}

function winCheck() {
    if (map[2][1].lastElementChild.classList.contains('box') &&
        map[3][5].lastElementChild.classList.contains('box') &&
        map[4][1].lastElementChild.classList.contains('box') &&
        map[5][4].lastElementChild.classList.contains('box') &&
        map[6][3].lastElementChild.classList.contains('box') &&
        map[6][6].lastElementChild.classList.contains('box') &&
        map[7][4].lastElementChild.classList.contains('box'))
        {
        document.getElementById('win').style.display = 'contents'  
    }
}
function reset() {
    window.location.reload()
}